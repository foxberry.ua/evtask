package com.foxberry.evtask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static com.foxberry.evtask.WorldsViewModel.PARAM_LOGIN;
import static com.foxberry.evtask.WorldsViewModel.PARAM_PASSWORD;

public class LoginActivity extends AppCompatActivity {
    private TextView loginTV;
    private TextView passwordTV;
    private View signInErrorTV;
    private View signInButton;
    public static final String SHARED_PREF_FILE = "signin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        signInButton.setOnClickListener((View v) -> {
                if (loginTV.getText() != null
                        && passwordTV.getText() != null
                        && loginTV.getText().toString().equals("android.test@xyrality.com")
                        && passwordTV.getText().toString().equals("password")) {
                    signInErrorTV.setVisibility(View.INVISIBLE);
                    SharedPreferences sharedPref = getApplication().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(PARAM_LOGIN, loginTV.getText().toString());
                    editor.putString(PARAM_PASSWORD, passwordTV.getText().toString());
                    editor.apply();
                    startActivity(new Intent(LoginActivity.this, WorldsActivity.class));
                } else {
                    signInErrorTV.setVisibility(View.VISIBLE);
                }
            }
        );
    }

    private void initViews() {
        SharedPreferences sharedPref = getApplication().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        loginTV = findViewById(R.id.login);
        passwordTV = findViewById(R.id.password);
        String lastLogin = sharedPref.getString(getString(R.string.login), null);
        String lastlPassword = sharedPref.getString(getString(R.string.password), null);
        if (lastLogin != null && lastlPassword != null) {
            loginTV.setText(lastLogin);
            passwordTV.setText(lastlPassword);
        }
        signInErrorTV = findViewById(R.id.signInError);
        signInButton = findViewById(R.id.signIn);
    }
}
