package com.foxberry.evtask;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class WorldsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    WorldsViewModel viewModel;
    WorldsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worlds);
        viewModel = ViewModelProviders.of(this).get(WorldsViewModel.class);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new WorldsAdapter();
        recyclerView.setAdapter(adapter);
        viewModel.getWorldsList().observe(this, worlds -> {
            adapter.setWorlds(worlds);
        });
    }
}
