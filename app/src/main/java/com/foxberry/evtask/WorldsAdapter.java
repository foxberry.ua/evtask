package com.foxberry.evtask;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Iryna Satsiuk on 2/24/18.
 */
public class WorldsAdapter extends RecyclerView.Adapter<WorldsAdapter.WorldHolder> {
    List<World> worlds = new ArrayList<>();

    @Override
    public WorldHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.world_row, parent, false);
        return new WorldHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WorldHolder holder, int position) {
        holder.name.setText(worlds.get(position).getName());
        holder.language.setText(worlds.get(position).getLanguage());
    }

    @Override
    public int getItemCount() {
        return worlds.size();
    }

    public void setWorlds(List<World> worlds) {
        this.worlds = worlds;
        notifyDataSetChanged();
    }

    class WorldHolder extends RecyclerView.ViewHolder {
        private TextView name, language;

        public WorldHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.worldName);
            language = itemView.findViewById(R.id.worldLanguage);
        }
    }
}
