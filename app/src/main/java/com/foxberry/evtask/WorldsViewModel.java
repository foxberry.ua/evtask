package com.foxberry.evtask;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static android.content.Context.WIFI_SERVICE;
import static com.foxberry.evtask.LoginActivity.SHARED_PREF_FILE;

/**
 * Created by Iryna Satsiuk on 2/24/18.
 */
public class WorldsViewModel extends AndroidViewModel {
    public static final String PARAM_LOGIN = "login";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_DEVICE_ID = "deviceId";
    public static final String PARAM_DEVICE_TYPE = "deviceType";
    private MutableLiveData<List<World>> worldsList;

    public WorldsViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<List<World>> getWorldsList() {
        if (worldsList == null) {
            worldsList = new MutableLiveData<>();
            loadWorldsList();
        }
        return worldsList;

    }

    private void loadWorldsList() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        String url = "http://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, response -> {
            Log.d("Response", response);
            try {
                JSONArray worldsArray = new JSONObject(response).getJSONArray("allAvailableWorlds");
                Gson gson = new Gson();
                worldsList.setValue(gson.fromJson(worldsArray.toString(), new TypeToken<ArrayList<World>>() {
                }.getType()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            Log.d("Error.Response", error.toString());
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sharedPref = getApplication().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
                params.put(PARAM_LOGIN, sharedPref.getString(PARAM_LOGIN, null));
                params.put(PARAM_PASSWORD, sharedPref.getString(PARAM_PASSWORD, null));
                params.put(PARAM_DEVICE_ID, createDeviceId());
                params.put(PARAM_DEVICE_TYPE, String.format("%s %s",
                        android.os.Build.MODEL, android.os.Build.VERSION.RELEASE));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        requestQueue.add(postRequest);

    }

    private String createDeviceId() {
        String result = ((WifiManager) getApplication().getApplicationContext()
                .getSystemService(WIFI_SERVICE)).getConnectionInfo().getMacAddress();
        return (result != null ? result : UUID.randomUUID().toString());
    }
}
